from django.shortcuts import render, get_object_or_404, redirect
from .models import Recipe
from django.utils import timezone
from recipes.forms import RecipeForm


def create_recipe(request):
    if request.method == "POST":
        submitted_form = RecipeForm(request.POST)
        if submitted_form.is_valid():
            submitted_form.save()
            return redirect("recipes_list")
    else:
        submitted_form = RecipeForm()
        
    form_dict = {
        "form": submitted_form,
    }
    return render(request, "recipes/create.html", form_dict)

def recipe_list(request):
    # (recipe_list)
    all_recipes = Recipe.objects.all()
    all_recipes_dict = {
        "all_recipes" : all_recipes
    }
    return render(request, "recipes/list.html", all_recipes_dict)


def show_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    content = {
        "recipe_object": recipe,
    }
    return render(request, "recipes/detail.html", content)

def edit_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    if request.method == "POST":
        submitted_form = RecipeForm(request.POST, instance=recipe)
        if submitted_form.is_valid():
            submitted_form.save()
            return redirect("show_list", id=id)
    else:
        submitted_form = RecipeForm(instance=recipe)
        
    form_dict = {
        "recipe_obj": recipe,
        "form": submitted_form
    }
    return render(request, "recipes/edit.html", form_dict)