from django.db import models
from django.utils import timezone

# Create your models here.
class Recipe(models.Model):
    title = models.CharField(max_length=200)
    picture = models.URLField()
    description = models.TextField()
    created_on = models.DateTimeField(auto_now_add=True)
    published_on = models.DateTimeField(blank=True, null=True)

    def publish(self):
        self.published_on = timezone.now()
        self.save()

    def __string__(self):
        return self.title


class RecipeStep(models.Model):
    instruction = models.TextField()
    order = models.PosititiveIntegerField()
    recipe = models.ForeignKey(
        "Recipe",
        related_name="steps",

    )

    def recipe_title(self):
        return self.recipe.title
    
    class Meta:
        ordering = ["order"]
