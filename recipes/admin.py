from django.contrib import admin
from recipes.models import Recipe
# could add more classes here

# myModels = (Recipe)

@admin.register(Recipe)
class RecipeAdmin(admin.ModelAdmin):
    list_display = ("title", "id")

# @admin.register(RecipeStep)
# class RecipeAdmin(admin.ModelAdmin):
#     list_display = ("recipe_title", "id")